import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppointmentModule } from './modules/appointment-module/appointment.module';
import { CabinetModule } from './modules/cabinet-module/cabinet.module';
import { ConsultationModule } from './modules/consultation-module/consultation.module';
import { PatientModule } from './modules/patient-module/patient.module';

@Module({
  imports: [
    MongooseModule.forRoot(`mongodb://localhost:27017/gcm`, { useNewUrlParser: true }),
    CabinetModule,
    PatientModule,
    ConsultationModule,
    AppointmentModule
    ],
  controllers: [],
  providers: [],
})
export class AppModule {}
