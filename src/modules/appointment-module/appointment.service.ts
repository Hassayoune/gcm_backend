import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { AppointmentDto } from "src/DTO/appointment.dto";
import { PatientDTO } from "src/DTO/patient.dto";
import { AppointmentInterface } from "src/interfaces/appointment.interface";
import { CabinetInterface } from "src/interfaces/cabinet.interface";
import { PatientInterface } from "src/interfaces/patient.interface";


@Injectable()
export class AppointmentService {
  constructor(
    @InjectModel('patient') private readonly patientModel: Model<PatientInterface>,
    @InjectModel('appointment') private readonly appointmentModel: Model<AppointmentInterface>,
  ) { }


  async createAppointment(data: AppointmentDto): Promise<AppointmentInterface> {
  
    const object = new this.appointmentModel(data);
    const result = await object.save();
    const patient = this.patientModel.findById(data.patient);
    await patient.updateOne({
        $addToSet: { appointments: result._id },
    })    

    return result;
  }

  async getPatientAppointments(id): Promise<AppointmentInterface[]>  {
    const result = await this.appointmentModel.find({patient:id});
    return result
  }


}
