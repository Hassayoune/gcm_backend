import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppointmentSchema } from 'src/schemas/appointment.schema';
import { PatientSchema } from 'src/schemas/patient.schema';
import { AppointmentController } from './appointment.controller';
import { AppointmentService } from './appointment.service';

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'appointment', schema: AppointmentSchema },
    { name: 'patient', schema: PatientSchema }
])
  ],
  controllers: [AppointmentController],
  providers: [AppointmentService],
})
export class AppointmentModule { }
