import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { ApiProperty, ApiTags } from "@nestjs/swagger";
import { AppointmentDto } from "src/DTO/appointment.dto";
import { AppointmentService } from "./appointment.service";




@ApiTags("Appointment")
@Controller('appointment')
export class AppointmentController {
  constructor(private readonly appointmentService: AppointmentService) { }

  @ApiProperty()
  @Post()
  async createAppointment(@Body() data: AppointmentDto) {
    const rs = await this.appointmentService.createAppointment(data);
    return rs
  }
  
  @ApiProperty()
  @Get(":idPatient")
  async getAllPatientAppointments(@Param('idPatient') id: string) {
    const rs = await this.appointmentService.getPatientAppointments(id);
    return rs
  }
}
