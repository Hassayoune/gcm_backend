import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CabinetSchema } from 'src/schemas/cabinet.schema';
import { CabinetController } from './cabinet.controller';
import { CabinetService } from './cabinet.service';


@Module({
  imports: [MongooseModule.forFeature([
    { name: 'cabinet', schema: CabinetSchema }])
  ],
  controllers: [CabinetController],
  providers: [CabinetService],
})
export class CabinetModule { }
