import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { ApiProperty, ApiTags } from "@nestjs/swagger";
import { CabinetDto } from "src/DTO/cabinet.dto";
import { CabinetService } from "./cabinet.service";



@ApiTags("Cabinet")
@Controller('cabinet')
export class CabinetController {
  constructor(private readonly cabinetService: CabinetService) { }

  @ApiProperty()
  @Post('/')
  async createCabinet(@Body() data: CabinetDto) {
    const rs = await this.cabinetService.createCabinet(data);
    return rs
  }


  @ApiProperty()
  @Get("/")
  async getAllCabinets() {
    const rs = await this.cabinetService.getAll();
    return rs
  }

  @ApiProperty()
  @Get(':id')
  async getSingleCabinet(@Param('id') id: string) {
    const rs = await this.cabinetService.getSingle(id);
    return rs
  }



}
