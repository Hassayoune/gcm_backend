import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { CabinetDto } from "src/DTO/cabinet.dto";
import { CabinetInterface } from "src/interfaces/cabinet.interface";


@Injectable()
export class CabinetService {
  constructor(
    @InjectModel('cabinet') private readonly cabinetModel: Model<CabinetInterface>,
  ) { }


  async createCabinet(data: CabinetDto): Promise<CabinetInterface> {
    const object = new this.cabinetModel(data)
    const result = await object.save();
    return result
  }

  async getAll(): Promise<CabinetInterface[]>  {

    const result = await this.cabinetModel.find()
    return result
  }

  async getSingle(id): Promise<CabinetInterface>  {

    const result = await this.cabinetModel.findById(id);
    return result
  }

}
