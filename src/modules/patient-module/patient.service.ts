import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { PatientDTO } from "src/DTO/patient.dto";
import { CabinetInterface } from "src/interfaces/cabinet.interface";
import { PatientInterface } from "src/interfaces/patient.interface";


@Injectable()
export class PatientService {
  constructor(
    @InjectModel('patient') private readonly patientModel: Model<PatientInterface>,
    @InjectModel('cabinet') private readonly cabinetModel: Model<CabinetInterface>,
  ) { }


  async createPatient(data: PatientDTO,id:String): Promise<PatientInterface> {
    const object = new this.patientModel(data)
    const result = await object.save();
    const cabinet = this.cabinetModel.findById(id);
    await cabinet.updateOne({
      $addToSet: { patients: result._id },
    })
    return result
  }

  async getAll(id): Promise<CabinetInterface>  {
    const patients = this.cabinetModel.findOne({doctorID:id}).populate({path:'patients', model: this.patientModel}).exec();
    return patients
  }

  async getSingle(id): Promise<PatientInterface>  {
    const result = await this.patientModel.findById(id);
    return result
  }

}
