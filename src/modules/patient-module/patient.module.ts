import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CabinetSchema } from 'src/schemas/cabinet.schema';
import { PatientSchema } from 'src/schemas/patient.schema';
import { PatientController } from './patient.controller';
import { PatientService } from './patient.service';



@Module({
  imports: [MongooseModule.forFeature([
    { name: 'patient', schema: PatientSchema },
    { name: 'cabinet', schema: CabinetSchema }
  ])
  ],
  controllers: [PatientController],
  providers: [PatientService],
})
export class PatientModule { }
