import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { ApiProperty, ApiTags } from "@nestjs/swagger";
import { PatientDTO } from "src/DTO/patient.dto";
import { PatientService } from "./patient.service";




@ApiTags("Patient")
@Controller('patient')
export class PatientController {
  constructor(private readonly patientService: PatientService) { }

  @ApiProperty()
  @Post('/:id')
  async createPatient(@Body() data: PatientDTO,@Param('id') id: string) {
    const rs = await this.patientService.createPatient(data,id);
    return rs
  }


  @ApiProperty()
  @Get("/me/:doctorID")
  async getAllPatients(@Param('doctorID') id: string) {
    const rs = await this.patientService.getAll(id);
    return rs
  }

  @ApiProperty()
  @Get(':patientID')
  async getSinglePatient(@Param('patientID') id: string) {
    const rs = await this.patientService.getSingle(id);
    return rs
  }



}
