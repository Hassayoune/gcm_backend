import { Body, Controller, Get, Param, Post, UseGuards } from "@nestjs/common";
import { ApiProperty, ApiTags } from "@nestjs/swagger";
import { ConsultationDTO } from "src/DTO/consultation.dto";
import { ConsultationService } from "./consultation.service";





@ApiTags("Consultation")
@Controller('consultation')
export class ConsultationController {
  constructor(private readonly consultationService: ConsultationService) { }

  @ApiProperty()
  @Post('/:id')
  async createPatient(@Body() data: ConsultationDTO,@Param('id') id: string) {
    const rs = await this.consultationService.createConsultation(data,id);
    return rs
  }


  @ApiProperty()
  @Get("/me/:patientID")
  async getAllConsultations(@Param('patientID') id: string) {
    const rs = await this.consultationService.getAll(id);
    return rs
  }

  @ApiProperty()
  @Get(':consultationID')
  async getSingleConsultation(@Param('consultationID') id: string) {
    const rs = await this.consultationService.getSingle(id);
    return rs
  }



}
