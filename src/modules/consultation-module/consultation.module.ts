import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConsultationSchema } from 'src/schemas/consultation.schema';
import { PatientSchema } from 'src/schemas/patient.schema';
import { ConsultationController } from './consultation.controller';
import { ConsultationService } from './consultation.service';




@Module({
  imports: [MongooseModule.forFeature([
    { name: 'patient', schema: PatientSchema },
    { name: 'consultation', schema: ConsultationSchema }
  ])
  ],
  controllers: [ConsultationController],
  providers: [ConsultationService],
})
export class ConsultationModule { }
