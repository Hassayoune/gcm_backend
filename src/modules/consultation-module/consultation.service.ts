import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { ConsultationDTO } from "src/DTO/consultation.dto";
import { ConsultationInterface } from "src/interfaces/consultation.interface";
import { PatientInterface } from "src/interfaces/patient.interface";


@Injectable()
export class ConsultationService {
  constructor(
    @InjectModel('patient') private readonly patientModel: Model<PatientInterface>,
    @InjectModel('consultation') private readonly consultationModel: Model<ConsultationInterface>,
  ) { }


  async createConsultation(data: ConsultationDTO,id:String): Promise<ConsultationInterface> {
    const object = new this.consultationModel(data)
    const result = await object.save();
    const patient = this.patientModel.findById(id);
    await patient.updateOne({
      $addToSet: { consultations: result._id },
    })
    return result
  }

  async getAll(id): Promise<PatientInterface>  {
    const consultations = this.patientModel.findById(id).populate({path:'consultations', model: this.consultationModel}).exec();
    return consultations
  }

  async getSingle(id): Promise<ConsultationInterface>  {
    const result = await this.consultationModel.findById(id);
    return result
  }

}
