import { Document } from 'mongoose';
import { PatientInterface } from './patient.interface';

export interface CabinetInterface extends Document {
    readonly title: string;
    readonly status: string;
    readonly doctorID: string;
    readonly secretaryID: string;
    readonly patients:PatientInterface[],
}
