import { Date, Document } from 'mongoose';
import { AppointmentInterface } from './appointment.interface';
import { ConsultationInterface } from './consultation.interface';

export interface PatientInterface extends Document {
    readonly title: string;
   readonly firstName: string;
   readonly lastName: string;
   readonly birthday: string;
   readonly cnam: string;
   readonly phone: string;
   readonly medRef:string;
   readonly note: string;
   readonly consultations:ConsultationInterface[],
   readonly appointments:AppointmentInterface[],

  
}
