import { Document } from 'mongoose';
import { PatientInterface } from './patient.interface';

export interface AppointmentInterface extends Document {
    readonly title: string;
    readonly status: string;
    readonly date: string;
    readonly doctorID : string;
    readonly patient:PatientInterface,
}
