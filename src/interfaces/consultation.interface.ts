import { Document } from 'mongoose';

export interface ConsultationInterface extends Document { 
readonly tension: string;
readonly ecg: string;
readonly weight: string;
readonly ht: string;
readonly glycemie: string;
readonly Treatment: string;


}
