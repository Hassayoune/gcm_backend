import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import {  IsDefined, IsNotEmpty, IsString, ValidateNested } from "class-validator";
import { PatientDTO } from "./patient.dto";

export class AppointmentDto {

@ApiProperty()
@IsString()
@IsDefined()
readonly title: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly status: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly date: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly doctorID: string;


@ApiProperty()
@Type(() => PatientDTO)
readonly patient: PatientDTO
}
