import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsString } from "class-validator";

export class ConsultationDTO {

@ApiProperty()
@IsString()
@IsDefined()
readonly tension: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly ecg: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly weight: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly ht: string;


@ApiProperty()
@IsString()
@IsDefined()
readonly glycemie: string;


@ApiProperty()
@IsString()
@IsDefined()
readonly Treatment: string;

}
