import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import {  IsDefined, IsNotEmpty, IsString, ValidateNested } from "class-validator";
import { PatientDTO } from "./patient.dto";

export class CabinetDto {

@ApiProperty()
@IsString()
@IsDefined()
readonly title: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly status: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly doctorID: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly secretaryID: string;

@ApiProperty()
@ValidateNested()
@Type(() => PatientDTO)
readonly patients: PatientDTO[]
}
