import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, IsString, ValidateNested } from "class-validator";
import { AppointmentDto } from "./appointment.dto";
import { ConsultationDTO } from "./consultation.dto";

export class PatientDTO {

@ApiProperty()
@IsString()
@IsDefined()
readonly firstName: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly lastName: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly birthday: string;

@ApiProperty()
@IsString()
@IsDefined()
readonly cnam: string;


@ApiProperty()
@IsString()
@IsDefined()
readonly phone: string;


@ApiProperty()
@IsString()
@IsDefined()
readonly medRef: string;

@ApiProperty()
@IsString()
readonly note: string;

@ApiProperty()
@ValidateNested()
@Type(() => ConsultationDTO)
readonly consultations: ConsultationDTO[]


@ApiProperty()
@ValidateNested()
@Type(() => AppointmentDto)
readonly appointments: AppointmentDto[]
}
