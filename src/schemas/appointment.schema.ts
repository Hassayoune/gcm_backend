
import { Schema } from 'mongoose';
import { AppointmentInterface } from 'src/interfaces/appointment.interface';

export const AppointmentSchema: Schema<AppointmentInterface> = new Schema<AppointmentInterface>({
  title: { type: String },
  status: { type: String },
  date: { type: String },
  doctorID: {type: String},
  patient:{ type: Schema.Types.ObjectId, ref: 'PatientSchema',required:false },
}, {
  timestamps: true
});
