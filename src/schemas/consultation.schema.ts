
import { Schema } from 'mongoose';
import { ConsultationInterface } from 'src/interfaces/consultation.interface';

export const ConsultationSchema: Schema<ConsultationInterface> = new Schema<ConsultationInterface>({
    tension: { type: String },
    ecg: { type: String },
    ht: { type: String },
    glycemie: { type: String },
    Treatment: { type: String }, 
}, {
  timestamps: true
});
