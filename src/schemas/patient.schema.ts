
import { Schema } from 'mongoose';
import { PatientInterface } from 'src/interfaces/patient.interface';

export const PatientSchema: Schema<PatientInterface> = new Schema<PatientInterface>({
    firstName: { type: String },
    lastName: { type: String },
    birthday: { type: String },
    cnam: { type: String },
    phone: { type: String },
    medRef: { type: String },
    note: { type: String },
    consultations: [{ type: Schema.Types.ObjectId, ref: 'ConsultationSchema',required:false }],
    appointments: [{ type: Schema.Types.ObjectId, ref: 'AppointmentSchema',required:false }],


}, {
    timestamps: true
  });
