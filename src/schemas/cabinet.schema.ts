
import { Schema } from 'mongoose';
import { CabinetInterface } from 'src/interfaces/cabinet.interface';

export const CabinetSchema: Schema<CabinetInterface> = new Schema<CabinetInterface>({
  title: { type: String },
  status: { type: String },
  doctorID: { type: String },
  secretaryID: { type: String },
  patients: [{ type: Schema.Types.ObjectId, ref: 'PatientSchema',required:false }],
}, {
  timestamps: true
});
